package io.aulanchik.patterns.creational.prototype;

import io.aulanchik.patterns.creational.prototype.impl.Shape;

public class PrototypeDemo {

    /*
     *	Prototype pattern refers to creating duplicate object while keeping performance in
     *	mind. This type of design pattern comes under creational pattern as this pattern
     *	provides one of the best ways to create an object.
     *	This pattern involves implementing a prototype interface which tells to create a
     *	clone of the current object. This pattern is used when creation of object directly is
     *	costly. For example, an object is to be created after a costly database operation.
     *	We can cache the object, return its clone on next request and update the database
     *	as and when needed thus reducing database calls.
     */

    public static void main(String[] args) throws CloneNotSupportedException {
        ShapeCache.loadCache();

        Shape clone = ShapeCache.getShape("1");
        System.out.println("Shape : " + clone.getType());

        Shape clone2 = ShapeCache.getShape("2");
        System.out.println("Shape : " + clone2.getType());

        Shape clone3 = ShapeCache.getShape("3");
        System.out.println("Shape : " + clone3.getType());
    }
}
