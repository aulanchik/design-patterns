package io.aulanchik.patterns.creational.prototype.impl;

public class Rectangle extends Shape {

    @Override
    void draw() {
        System.out.println("Inside Rectangle::draw() method");
    }
}
