package io.aulanchik.patterns.creational.prototype.impl;

public class Circle extends Shape {

    @Override
    void draw() {
        System.out.println("Inside Circle::draw() method");
    }
}
