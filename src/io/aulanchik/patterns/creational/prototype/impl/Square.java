package io.aulanchik.patterns.creational.prototype.impl;

public class Square extends Shape {

    @Override
    void draw() {
        System.out.println("Inside Square::draw() method");
    }
}
