package io.aulanchik.patterns.creational.prototype;

import io.aulanchik.patterns.creational.prototype.impl.Circle;
import io.aulanchik.patterns.creational.prototype.impl.Rectangle;
import io.aulanchik.patterns.creational.prototype.impl.Shape;
import io.aulanchik.patterns.creational.prototype.impl.Square;

import java.util.Hashtable;

class ShapeCache {

    private static Hashtable<String, Shape> shapeMap = new Hashtable<String, Shape>();

    static Shape getShape(String shapeId) throws CloneNotSupportedException {
        Shape cachedShape = shapeMap.get(shapeId);
        return cachedShape.clone();
    }

    // for each shape run database query and create shape
    // shapeMap.put(shapeKey, shape);
    // for example, we are adding three shapes

    static void loadCache() {
        Circle circle = new Circle();
        circle.setId("1");

        shapeMap.put(circle.getId(), circle);
        Square square = new Square();
        square.setId("2");

        shapeMap.put(square.getId(), square);
        Rectangle rectangle = new Rectangle();
        rectangle.setId("3");

        shapeMap.put(rectangle.getId(), rectangle);
    }

}
