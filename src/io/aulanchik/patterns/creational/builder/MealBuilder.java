package io.aulanchik.patterns.creational.builder;

import io.aulanchik.patterns.creational.builder.impl.ChickenSandwich;
import io.aulanchik.patterns.creational.builder.impl.Meal;
import io.aulanchik.patterns.creational.builder.impl.VegSandwich;

class MealBuilder {

    Meal orderVegMeal() {
        Meal meal = new Meal();
        meal.addItem(new VegSandwich());
        return meal;
    }

    Meal prepareNonVegMeal() {
        Meal meal = new Meal();
        meal.addItem(new ChickenSandwich());
        return meal;
    }

}
