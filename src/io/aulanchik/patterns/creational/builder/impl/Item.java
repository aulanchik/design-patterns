package io.aulanchik.patterns.creational.builder.impl;

public interface Item {
    String name();

    Packing packing();

    float price();
}
