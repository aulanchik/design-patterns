package io.aulanchik.patterns.creational.builder.impl;

public class Bottle implements Packing {

    @Override
    public String pack() {
        return "Bottle";
    }
}
