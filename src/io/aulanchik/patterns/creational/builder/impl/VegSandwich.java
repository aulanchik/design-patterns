package io.aulanchik.patterns.creational.builder.impl;

public class VegSandwich extends Sandwich {

    @Override
    public String name() {
        return "Vegan Sandwich";
    }

    @Override
    public float price() {
        return 25.0f;
    }
}
