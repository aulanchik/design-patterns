package io.aulanchik.patterns.creational.builder.impl;

public interface Packing {
    String pack();
}
