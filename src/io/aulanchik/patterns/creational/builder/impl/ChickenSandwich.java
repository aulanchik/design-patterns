package io.aulanchik.patterns.creational.builder.impl;

public class ChickenSandwich extends Sandwich {

    @Override
    public String name() {
        return "Chicken Sandwich";
    }

    @Override
    public float price() {
        return 12.0f;
    }
}
