package io.aulanchik.patterns.creational.builder.impl;

public abstract class Sandwich implements Item {

    @Override
    public String name() {
        return "";
    }

    @Override
    public Packing packing() {
        return new Wrapper();
    }

    public abstract float price();
}
