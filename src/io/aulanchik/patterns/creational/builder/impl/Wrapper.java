package io.aulanchik.patterns.creational.builder.impl;

public class Wrapper implements Packing {
    @Override
    public String pack() {
        return "Wrapper";
    }
}
