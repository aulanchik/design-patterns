package io.aulanchik.patterns.creational.abstractfactory;

import io.aulanchik.patterns.creational.abstractfactory.factory.impl.color.Color;
import io.aulanchik.patterns.creational.abstractfactory.factory.impl.shape.Shape;

public interface AbstractFactory {
    Color getColor(String color);

    Shape getShape(String shape);
}
