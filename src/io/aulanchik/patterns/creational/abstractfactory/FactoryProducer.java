package io.aulanchik.patterns.creational.abstractfactory;

import io.aulanchik.patterns.creational.abstractfactory.factory.ColorFactory;
import io.aulanchik.patterns.creational.abstractfactory.factory.ShapeFactory;

class FactoryProducer {
    static AbstractFactory getFactory(String choice) {
        switch (choice) {
            case "SHAPE":
                return new ShapeFactory();
            case "COLOR":
                return new ColorFactory();
            default:
                return null;
        }
    }
}