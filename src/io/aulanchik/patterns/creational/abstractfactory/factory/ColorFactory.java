package io.aulanchik.patterns.creational.abstractfactory.factory;

import io.aulanchik.patterns.creational.abstractfactory.AbstractFactory;
import io.aulanchik.patterns.creational.abstractfactory.factory.impl.color.Blue;
import io.aulanchik.patterns.creational.abstractfactory.factory.impl.color.Color;
import io.aulanchik.patterns.creational.abstractfactory.factory.impl.color.Green;
import io.aulanchik.patterns.creational.abstractfactory.factory.impl.color.Red;
import io.aulanchik.patterns.creational.abstractfactory.factory.impl.shape.Shape;

public class ColorFactory implements AbstractFactory {

    @Override
    public Color getColor(String color) {
        switch (color) {
            case "RED":
                return new Red();
            case "GREEN":
                return new Green();
            case "BLUE":
                return new Blue();
            default:
                return null;
        }
    }

    @Override
    public Shape getShape(String shape) {
        return null;
    }
}
