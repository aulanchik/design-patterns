package io.aulanchik.patterns.creational.abstractfactory.factory.impl.color;

public class Blue implements Color {

    @Override
    public void fill() {
        System.out.println("This is Blue.");
    }
}
