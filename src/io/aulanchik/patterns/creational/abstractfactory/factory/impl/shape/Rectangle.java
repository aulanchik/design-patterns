package io.aulanchik.patterns.creational.abstractfactory.factory.impl.shape;

public class Rectangle implements Shape {

    @Override
    public void draw() {
        System.out.println("This is Rectangle.");
    }

}
