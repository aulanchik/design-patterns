package io.aulanchik.patterns.creational.abstractfactory.factory.impl.shape;

public class Square implements Shape {

    @Override
    public void draw() {
        System.out.println("This is Square.");
    }

}
