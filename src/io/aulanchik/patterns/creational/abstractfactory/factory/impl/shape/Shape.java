package io.aulanchik.patterns.creational.abstractfactory.factory.impl.shape;

public interface Shape {
    void draw();
}
