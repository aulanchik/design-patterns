package io.aulanchik.patterns.creational.abstractfactory.factory.impl.color;

public interface Color {
    void fill();
}
