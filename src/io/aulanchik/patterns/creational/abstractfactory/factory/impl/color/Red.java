package io.aulanchik.patterns.creational.abstractfactory.factory.impl.color;

public class Red implements Color {

    @Override
    public void fill() {
        System.out.println("This is Red.");
    }
}
