package io.aulanchik.patterns.creational.abstractfactory.factory.impl.color;

public class Green implements Color {

    @Override
    public void fill() {
        System.out.println("This is Green.");
    }
}
