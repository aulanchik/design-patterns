package io.aulanchik.patterns.creational.abstractfactory.factory;

import io.aulanchik.patterns.creational.abstractfactory.AbstractFactory;
import io.aulanchik.patterns.creational.abstractfactory.factory.impl.color.Color;
import io.aulanchik.patterns.creational.abstractfactory.factory.impl.shape.Rectangle;
import io.aulanchik.patterns.creational.abstractfactory.factory.impl.shape.Shape;
import io.aulanchik.patterns.creational.abstractfactory.factory.impl.shape.Square;
import io.aulanchik.patterns.creational.abstractfactory.factory.impl.shape.Triangle;


public class ShapeFactory implements AbstractFactory {

    @Override
    public Color getColor(String color) {
        return null;
    }

    @Override
    public Shape getShape(String shape) {
        switch (shape) {
            case "RECTANGLE":
                return new Rectangle();
            case "TRIANGLE":
                return new Triangle();
            case "SQUARE":
                return new Square();
            default:
                return null;
        }
    }
}
