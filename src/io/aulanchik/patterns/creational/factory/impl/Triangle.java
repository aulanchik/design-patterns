package io.aulanchik.patterns.creational.factory.impl;

public class Triangle implements Shape {
    @Override
    public void draw() {
        System.out.println("This is Triangle.");
    }
}
