package io.aulanchik.patterns.creational.factory.impl;

public class Square implements Shape {
    @Override
    public void draw() {
        System.out.println("This is Square.");
    }
}
