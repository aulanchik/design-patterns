package io.aulanchik.patterns.creational.factory.impl;

public interface Shape {
    void draw();
}
