package io.aulanchik.patterns.creational.factory;

import io.aulanchik.patterns.creational.factory.impl.Circle;
import io.aulanchik.patterns.creational.factory.impl.Shape;
import io.aulanchik.patterns.creational.factory.impl.Square;
import io.aulanchik.patterns.creational.factory.impl.Triangle;

class ShapeFactory {
    Shape getShape(String shapeType) {
        switch (shapeType) {
            case "CIRCLE":
                return new Circle();
            case "TRIANGLE":
                return new Triangle();
            case "SQUARE":
                return new Square();
            default:
                return null;
        }
    }
}
