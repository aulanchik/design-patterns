package io.aulanchik.patterns.structural.facade.impl;

public interface Shape {
    void draw();
}
