package io.aulanchik.patterns.structural.facade.impl;

public class Circle implements Shape {

    @Override
    public void draw() {
        System.out.println("Shape: Circle");
    }
}
