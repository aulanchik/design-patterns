package io.aulanchik.patterns.structural.proxy.impl;

public interface Image {
    void display();
}
