package io.aulanchik.patterns.structural.proxy;

import io.aulanchik.patterns.structural.proxy.impl.Image;
import io.aulanchik.patterns.structural.proxy.impl.ProxyImage;

public class ProxyDemo {


    /*
     * In proxy pattern, a class represents functionality of another class. This type of
     * design pattern comes under structural pattern.
     * In proxy pattern, we create object having original object to interface its
     * functionality to outer world.
     *
     */

    public static void main(String[] args) {
        Image image = new ProxyImage("test_image.jpg");
        //load & display image from disk
        image.display();

        //image won't be loaded from a disk
        System.out.println("===");
        image.display();
    }

}
