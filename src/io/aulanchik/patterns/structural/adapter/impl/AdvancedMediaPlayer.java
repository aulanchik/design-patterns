package io.aulanchik.patterns.structural.adapter.impl;

public interface AdvancedMediaPlayer {
    void playVlc(String fileName);

    void playMp4(String fileName);
}
