package io.aulanchik.patterns.structural.adapter.impl;

public class MediaAdapter implements MediaPlayer {

    AdvancedMediaPlayer advMPlayer;

    public MediaAdapter(String audioType) {
        if (audioType.equalsIgnoreCase("vlc")) {
            advMPlayer = new VLCPlayer();
        } else if (audioType.equalsIgnoreCase("mp4")) {
            advMPlayer = new Mp4Player();
        }
    }

    @Override
    public void play(String fileType, String fileName) {
        if (fileType.equalsIgnoreCase("vlc")) {
            advMPlayer.playVlc(fileName);
        } else if (fileType.equalsIgnoreCase("mp4")) {
            advMPlayer.playMp4(fileName);
        }
    }
}
