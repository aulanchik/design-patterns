package io.aulanchik.patterns.structural.adapter.impl;

public interface MediaPlayer {
    void play(String fileType, String fileName);
}
