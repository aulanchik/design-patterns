package io.aulanchik.patterns.structural.adapter.impl;

public class AudioPlayer implements MediaPlayer {

    MediaAdapter mediaAdapter;

    @Override
    public void play(String fileType, String fileName) {

        // inbuilt support to play mp3 music files
        if (fileType.equalsIgnoreCase("mp3")) {
            System.out.println("Playing mp3 file. Name: " + fileName);
        } // mediaAdapter is providing support to play other file formats
        else if (fileType.equalsIgnoreCase("vlc") || fileType.equalsIgnoreCase("mp4")) {
            mediaAdapter = new MediaAdapter(fileType);
            mediaAdapter.play(fileType, fileName);
        } else {
            System.out.println("Invalid media. " + fileType + " format not supported");
        }

    }
}
