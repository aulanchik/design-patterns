package io.aulanchik.patterns.structural.flyweight.impl;

import java.util.List;

public class RandomUtils {

    public static int getRandomInt() {
        return (int) (Math.random() * 100);
    }

    public static String getRandomColor(List<String> colors) {
        return colors.get((int) (Math.random() * colors.size()));
    }
}
