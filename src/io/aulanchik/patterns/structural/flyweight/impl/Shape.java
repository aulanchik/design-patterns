package io.aulanchik.patterns.structural.flyweight.impl;

public interface Shape {
    void draw();
}
