package io.aulanchik.patterns.structural.flyweight;

import io.aulanchik.patterns.structural.flyweight.impl.Circle;
import io.aulanchik.patterns.structural.flyweight.impl.RandomUtils;
import io.aulanchik.patterns.structural.flyweight.impl.ShapeFactory;

import java.util.Arrays;
import java.util.List;

public class FlyweightDemo {

    /*
     * Flyweight pattern is primarily used to reduce the number of objects created and
     * to decrease memory footprint and increase performance. This type of design
     * pattern comes under structural pattern as this pattern provides ways to decrease
     * object count thus improving the object structure of application.
     * Flyweight pattern tries to reuse already existing similar kind objects by storing
     * them and creates new object when no matching object is found. We will
     * demonstrate this pattern by drawing 20 circles of different locations but we will
     * create only 5 objects. Only 5 colors are available so color property is used to check
     * already existing Circle objects.
     */

    private static List<String> COLORS = Arrays.asList("Red", "Green", "Blue", "White", "Black");

    public static void main(String[] args) {
        for (int i = 0; i < RandomUtils.getRandomInt(); i++) {
            Circle circle = (Circle) ShapeFactory.getCircle(RandomUtils.getRandomColor(COLORS));
            circle.setX(RandomUtils.getRandomInt());
            circle.setY(RandomUtils.getRandomInt());
            circle.setRadius(100);
            circle.draw();
        }
    }


}
