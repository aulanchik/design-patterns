package io.aulanchik.patterns.structural.composite;

import io.aulanchik.patterns.structural.composite.impl.Department;
import io.aulanchik.patterns.structural.composite.impl.FinancialDepartment;
import io.aulanchik.patterns.structural.composite.impl.HeadDepartment;
import io.aulanchik.patterns.structural.composite.impl.SalesDepartment;

public class CompositeDemo {

    /*
     * Composite pattern is used where we need to treat a group of objects in similar
     * way as a single object. Composite pattern composes objects in term of a tree
     * structure to represent part as well as whole hierarchy. This type of design pattern
     * comes under structural pattern as this pattern creates a tree structure of a group
     * of objects.
     * This pattern creates a class that contains a group of its own objects. This class
     * provides ways to modify its group of same objects.
     * We are demonstrating use of composite pattern via following example in which we
     * will show employees hierarchy of an organization.
     */

    public static void main(String[] args) {
        Department salesDepartment = new SalesDepartment(1, "Sales department");
        Department financialDepartment = new FinancialDepartment(2, "Financial department");
        HeadDepartment headDepartment = new HeadDepartment(3, "Head department");

        headDepartment.addDepartment(salesDepartment);
        headDepartment.addDepartment(financialDepartment);

        headDepartment.printDepartmentName();
    }
}
