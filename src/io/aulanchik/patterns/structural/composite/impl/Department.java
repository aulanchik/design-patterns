package io.aulanchik.patterns.structural.composite.impl;

public interface Department {
    void printDepartmentName();
}
