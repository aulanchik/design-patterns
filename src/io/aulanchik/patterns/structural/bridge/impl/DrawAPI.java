package io.aulanchik.patterns.structural.bridge.impl;

public interface DrawAPI {
    void drawCircle(int radius, int x, int y);
}
