package io.aulanchik.patterns.structural.decorator;

import io.aulanchik.patterns.structural.decorator.impl.Shape;

public abstract class ShapeDecorator implements Shape {

    protected Shape decoShape;

    public ShapeDecorator(Shape decoShape) {
        this.decoShape = decoShape;
    }

    @Override
    public void draw() {
        decoShape.draw();
    }
}
