package io.aulanchik.patterns.structural.decorator;

import io.aulanchik.patterns.structural.decorator.impl.Shape;

public class RedShapeDecorator extends ShapeDecorator {

    public RedShapeDecorator(Shape decoShape) {
        super(decoShape);
    }

    @Override
    public void draw() {
        super.draw();
        setRedBorder(decoShape);
    }

    private void setRedBorder(Shape decoShape) {
        System.out.println("Border Color: Red");
    }
}
