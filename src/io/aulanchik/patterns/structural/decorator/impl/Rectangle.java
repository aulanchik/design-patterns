package io.aulanchik.patterns.structural.decorator.impl;

public class Rectangle implements Shape {

    @Override
    public void draw() {
        System.out.println("Shape: Rectangle");
    }
}
