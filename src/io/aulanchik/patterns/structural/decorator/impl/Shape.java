package io.aulanchik.patterns.structural.decorator.impl;

public interface Shape {
    void draw();
}
