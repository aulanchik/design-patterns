package io.aulanchik.patterns.behavioral.iterator;

public class ObjectRepository implements Container {

    private String[] names = {"Robert", "Arthur", "Jess", "Lora"};

    @Override
    public Iterator getIterator() {
        return new ObjectIterator();
    }

    private class ObjectIterator implements Iterator {

        private int index;

        @Override
        public boolean hasNext() {
            return index < names.length;
        }

        @Override
        public Object next() {

            if (this.hasNext()) {
                return names[index++];
            }
            return null;
        }
    }
}
