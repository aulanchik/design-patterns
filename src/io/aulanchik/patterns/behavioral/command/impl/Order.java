package io.aulanchik.patterns.behavioral.command.impl;

public interface Order {
    void execute();
}
