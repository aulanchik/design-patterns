package io.aulanchik.patterns.behavioral.command.impl;

public class Stock {

    private String name = "StockName";
    private int quantity = 5;

    public void buy() {
        System.out.println("Stock [ Name: " + name + ", Quantity: " + quantity + " ] bought");
    }

    public void sell() {
        System.out.println("Stock [ Name: " + name + ",Quantity: " + quantity + " ] sold");
    }

}
