package io.aulanchik.patterns.behavioral.command;

import io.aulanchik.patterns.behavioral.command.impl.BuyStock;
import io.aulanchik.patterns.behavioral.command.impl.SellStock;
import io.aulanchik.patterns.behavioral.command.impl.Stock;

public class CommandDemo {

    /*
     * Command pattern is a data driven design pattern and falls under behavioral
     * pattern category. A request is wrapped under an object as command and passed
     * to invoker object. Invoker object looks for the appropriate object which can handle
     * this command and passes the command to the corresponding object which
     * executes it.
     *
     */

    public static void main(String[] args) {
        Stock stock = new Stock();

        BuyStock buyOrder = new BuyStock(stock);
        SellStock sellOrder = new SellStock(stock);

        Broker broker = new Broker();
        broker.takeOrder(buyOrder);
        broker.takeOrder(sellOrder);

        broker.placeOrder();
    }
}
