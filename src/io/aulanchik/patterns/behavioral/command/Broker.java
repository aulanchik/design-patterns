package io.aulanchik.patterns.behavioral.command;

import io.aulanchik.patterns.behavioral.command.impl.Order;

import java.util.ArrayList;
import java.util.List;

public class Broker {

    private List<Order> orders = new ArrayList<>();

    public void takeOrder(Order order) {
        orders.add(order);
    }

    public void placeOrder() {
        orders.forEach(Order::execute);
        orders.clear();
    }
}
