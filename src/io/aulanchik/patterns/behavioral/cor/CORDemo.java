package io.aulanchik.patterns.behavioral.cor;

import io.aulanchik.patterns.behavioral.cor.impl.ConsoleLogger;
import io.aulanchik.patterns.behavioral.cor.impl.ErrorLogger;
import io.aulanchik.patterns.behavioral.cor.impl.FileLogger;
import io.aulanchik.patterns.behavioral.cor.impl.Logger;

public class CORDemo {

    /*
     * Chain Of Responsibillity Pattern
     *
     * As the name suggests, the chain of responsibility pattern creates a chain of
     * receiver objects for a request. This pattern decouples sender and receiver of a
     * request based on type of request. This pattern comes under behavioral patterns.
     * In this pattern, normally each receiver contains reference to another receiver. If
     * one object cannot handle the request then it passes the same to the next receiver
     * and so on.
     *
     */

    private static Logger getChainOfLoggers() {
        Logger errorLogger = new ErrorLogger(Logger.ERROR);
        Logger fileLogger = new FileLogger(Logger.DEBUG);
        Logger consoleLogger = new ConsoleLogger(Logger.INFO);

        errorLogger.setNextLogger(fileLogger);
        fileLogger.setNextLogger(consoleLogger);

        return errorLogger;
    }

    public static void main(String[] args) {
        Logger loggerChain = getChainOfLoggers();
        loggerChain.logMessage(Logger.INFO, "This is an information.");
        loggerChain.logMessage(Logger.DEBUG, "This is a debug level information.");
        loggerChain.logMessage(Logger.ERROR, "This is an error information.");
    }
}
