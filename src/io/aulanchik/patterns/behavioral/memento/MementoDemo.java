package io.aulanchik.patterns.behavioral.memento;

public class MementoDemo {
    /*
     * Memento pattern is used to restore state of an object to a previous state.
     */

    public static void main(String[] args) {
        Originator originator = new Originator();
        CareTaker careTaker = new CareTaker();

        originator.setState("State #1");
        careTaker.add(originator.saveState());

        originator.setState("State #2");
        careTaker.add(originator.saveState());

        System.out.println("Current State: " + originator.getState());
        originator.getState(careTaker.get(0));

        System.out.println("First saved State: " + originator.getState());

        originator.getState(careTaker.get(1));
        System.out.println("Second saved State: " + originator.getState());
    }
}
