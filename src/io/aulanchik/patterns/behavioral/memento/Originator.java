package io.aulanchik.patterns.behavioral.memento;

public class Originator {
    private String state;

    public Memento saveState() {
        return new Memento(state);
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void getState(Memento memento) {
        state = memento.getState();
    }
}
