package io.aulanchik.patterns.behavioral.interpreter;

import io.aulanchik.patterns.behavioral.interpreter.impl.AndExpression;
import io.aulanchik.patterns.behavioral.interpreter.impl.Expression;
import io.aulanchik.patterns.behavioral.interpreter.impl.OrExpression;
import io.aulanchik.patterns.behavioral.interpreter.impl.TerminalExpression;

public class ExpressionRules {

    // Rule: Robert and John are males
    public static Expression getMales() {
        Expression robert = new TerminalExpression("Robert");
        Expression john = new TerminalExpression("John");
        return new OrExpression(robert, john);
    }

    // Rule: Julie is a married womens
    public static Expression getMarriedWomans() {
        Expression julie = new TerminalExpression("Julie");
        Expression married = new TerminalExpression("Married");
        return new AndExpression(julie, married);
    }

}
