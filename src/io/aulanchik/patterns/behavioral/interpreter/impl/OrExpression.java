package io.aulanchik.patterns.behavioral.interpreter.impl;

public class OrExpression implements Expression {

    private Expression exp1 = null;
    private Expression exp2 = null;

    public OrExpression(Expression exp1, Expression exp2) {
        this.exp1 = exp1;
        this.exp2 = exp2;
    }

    @Override
    public boolean interpretation(String context) {
        return exp1.interpretation(context) || exp2.interpretation(context);
    }
}
