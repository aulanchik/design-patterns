package io.aulanchik.patterns.behavioral.interpreter.impl;

public interface Container {
    Iterator getIterator();
}
