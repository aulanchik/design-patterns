package io.aulanchik.patterns.behavioral.interpreter.impl;

public interface Expression {
    boolean interpretation(String context);
}
