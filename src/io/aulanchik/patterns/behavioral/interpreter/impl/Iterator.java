package io.aulanchik.patterns.behavioral.interpreter.impl;

public interface Iterator {
    boolean hasNext();

    Object next();
}
