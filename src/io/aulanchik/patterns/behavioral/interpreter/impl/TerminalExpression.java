package io.aulanchik.patterns.behavioral.interpreter.impl;

public class TerminalExpression implements Expression {

    private String data;

    public TerminalExpression(String data) {
        this.data = data;
    }

    @Override
    public boolean interpretation(String context) {
        return context.contains(data);
    }
}
