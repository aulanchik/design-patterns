package io.aulanchik.patterns.behavioral.template;

import io.aulanchik.patterns.behavioral.template.impl.BasketBall;
import io.aulanchik.patterns.behavioral.template.impl.FootBall;
import io.aulanchik.patterns.behavioral.template.impl.Game;

public class TemplateDemo {

    /*
     * In Template pattern, an abstract class
     * exposes defined way(s)/template(s) to execute its methods.
     * Its subclasses can override the method implementation
     * as per need but the invocation is to be in the
     * same way as defined by an abstract class.
     */

    public static void main(String[] args) {
        Game game = new BasketBall();
        game.play();

        System.out.println();

        game = new FootBall();
        game.play();

        System.out.println();
    }
}
