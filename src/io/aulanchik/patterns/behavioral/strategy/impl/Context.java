package io.aulanchik.patterns.behavioral.strategy.impl;

public class Context {
    private Operation operation;

    public Context(Operation operation) {
        this.operation = operation;
    }

    public int execute(int num1, int num2) {
        return operation.perform(num1, num2);
    }
}
