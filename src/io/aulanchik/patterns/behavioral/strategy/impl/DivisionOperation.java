package io.aulanchik.patterns.behavioral.strategy.impl;

public class DivisionOperation implements Operation {

    @Override
    public int perform(int arg1, int arg2) {
        return arg1 / arg2;
    }
}
