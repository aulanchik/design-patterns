package io.aulanchik.patterns.behavioral.strategy.impl;

public interface Operation {
    int perform(int arg1, int arg2);
}
