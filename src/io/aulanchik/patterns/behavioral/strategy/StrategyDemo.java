package io.aulanchik.patterns.behavioral.strategy;

import io.aulanchik.patterns.behavioral.strategy.impl.AddOperation;
import io.aulanchik.patterns.behavioral.strategy.impl.Context;
import io.aulanchik.patterns.behavioral.strategy.impl.MultiplyOperator;
import io.aulanchik.patterns.behavioral.strategy.impl.SubstractOperation;

public class StrategyDemo {

    /**
     * In Strategy pattern, we create objects which represent various strategies and
     * a context object whose behavior varies as per its strategy object.
     * The strategy object changes the executing algorithm of the context object.
     */

    public static void main(String[] args) {
        Context context = new Context(new AddOperation());
        System.out.println("10 + 5 = " + context.execute(10, 5));

        context = new Context(new SubstractOperation());
        System.out.println("10 - 5 = " + context.execute(10, 5));

        context = new Context(new MultiplyOperator());
        System.out.println("10 * 5 = " + context.execute(10, 5));
    }
}
