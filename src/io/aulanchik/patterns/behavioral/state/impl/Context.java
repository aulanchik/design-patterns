package io.aulanchik.patterns.behavioral.state.impl;

public class Context {

    private static volatile Context instance = null;
    private State state;

    private Context() {
    }

    public static Context getContext() {
        if (instance == null) {
            synchronized (Context.class) {
                if (instance == null) {
                    instance = new Context();
                }
            }
        }
        return instance;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
}
