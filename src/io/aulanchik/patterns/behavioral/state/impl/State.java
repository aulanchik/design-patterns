package io.aulanchik.patterns.behavioral.state.impl;

public interface State {
    void doAction(Context context);
}
