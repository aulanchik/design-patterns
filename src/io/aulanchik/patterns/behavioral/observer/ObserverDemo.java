package io.aulanchik.patterns.behavioral.observer;

import io.aulanchik.patterns.behavioral.observer.impl.BinaryObserver;
import io.aulanchik.patterns.behavioral.observer.impl.Observer;
import io.aulanchik.patterns.behavioral.observer.impl.OctaObserver;
import io.aulanchik.patterns.behavioral.observer.impl.Subject;


public class ObserverDemo {

    /*
     * Observer pattern uses three actor classes.Subject, Observer and Client.
     * Subject is an object having methods to attach and detach observers to a client object.
     */

    private static int PAYLOAD = 5;

    public static void main(String[] args) {
        Subject subject = new Subject();
        Observer biO = new BinaryObserver(subject);
        Observer octaO = new OctaObserver(subject);

        subject.setState(PAYLOAD);

        subject.notifyAllObservers();

    }

}
