package io.aulanchik.patterns.behavioral.observer.impl;

public class OctaObserver extends Observer {

    public OctaObserver(Subject subject) {
        this.subject = subject;
        this.subject.attach(this);
    }

    @Override
    void update() {
        System.out.println("Octal String is: " + Integer.toOctalString(subject.getState()));
    }
}
