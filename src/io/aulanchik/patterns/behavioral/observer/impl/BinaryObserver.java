package io.aulanchik.patterns.behavioral.observer.impl;

public class BinaryObserver extends Observer {

    public BinaryObserver(Subject subject) {
        this.subject = subject;
        this.subject.attach(this);
    }

    @Override
    void update() {
        System.out.println("Binary String is: " + Integer.toBinaryString(subject.getState()));
    }
}
